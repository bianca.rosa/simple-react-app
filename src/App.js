import "./App.css";

function App() {
  return (
    <div className="App">
      <header className="App-header">
        <h1>This is a simple react app</h1>
        <p>Welcome to my react app</p>
      </header>
    </div>
  );
}

export default App;
