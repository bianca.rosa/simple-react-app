import { render, screen } from "@testing-library/react";
import App from "./App";

test("renders simple react app title", () => {
  render(<App />);
  const headerElement = screen.getByText(/This is a simple react app/i);
  expect(headerElement).toBeInTheDocument();
});

test("renders welcome paragraph", () => {
  render(<App />);
  const paragraphElement = screen.getByText(/Welcome to my react app/i);
  expect(paragraphElement).toBeInTheDocument();
});
