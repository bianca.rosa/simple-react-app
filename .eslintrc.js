module.exports = {
  env: {
    browser: true, // Enables browser globals like window and document
    es2021: true, // Supports modern ES features
    node: true, // Enables Node.js globals and CommonJS
    jest: true, // Enables Jest testing framework globals like test and expect
  },
  extends: [
    "eslint:recommended",
    "plugin:react/recommended",
    "plugin:react-hooks/recommended",
  ],
  parserOptions: {
    ecmaFeatures: {
      jsx: true, // Enables parsing of JSX
    },
    ecmaVersion: "latest", // Supports the latest ECMAScript version
    sourceType: "module", // Supports ES modules
  },
  plugins: ["react"],
  rules: {
    "no-unused-vars": [
      "error",
      { vars: "all", args: "none", ignoreRestSiblings: false },
    ], // Reports unused variables
    "react/react-in-jsx-scope": "off", // Not required for React 17+
    "react/prop-types": "off", // Disable PropTypes checks if not using them
  },
  settings: {
    react: {
      version: "detect", // Automatically detect React version
    },
  },
};
