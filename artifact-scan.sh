#!/bin/bash

# Configuration
ARTIFACT="simple-react-app.tar.cz"
METASCAN_API="https://api.metadefender.com/v4"
API_KEY=$OPSWAT_API_KEY
POLL_INTERVAL=10

# Check if the artifact exists
if [ ! -f "$ARTIFACT" ]; then
    echo "Error: Artifact '$ARTIFACT' not found."
    exit 1
fi

# Calculate SHA-256 hash
HASH=$(sha256sum "$ARTIFACT" | awk '{print $1}')
echo "SHA-256 hash: $HASH"

# Perform hash lookup using MetaDefender API
echo "Checking hash on MetaDefender..."
RESPONSE=$(curl -s -X GET "$METASCAN_API/hash/$HASH" \
    -H "apikey: $API_KEY" \
    -H "Content-Type: application/json")
if [ $? -ne 0 ]; then
    echo "Error: Hash lookup failed."
    exit 1
fi
echo "Hash Lookup Response: $RESPONSE"

# Check if the hash is found
if echo "$RESPONSE" | grep -q '"found":true'; then
    echo "Cached result found."
    SCAN_RESULT=$(echo "$RESPONSE" | jq -r '.scan_results.scan_all_result_a')
    echo "Scan Result: $SCAN_RESULT"
else
    echo "No cached result found. Uploading the file..."
    
    # Upload the file
    UPLOAD_RESPONSE=$(curl -s -X POST "$METASCAN_API/file" \
        -H "apikey: $API_KEY" \
        -H "Content-Type: multipart/form-data" \
        -F "file=@$ARTIFACT")
    if [ $? -ne 0 ] || [ -z "$UPLOAD_RESPONSE" ]; then
        echo "Error: File upload failed."
        exit 1
    fi
    echo "Upload Response: $UPLOAD_RESPONSE"

    # Extract the Data ID
    DATA_ID=$(echo "$UPLOAD_RESPONSE" | jq -r '.data_id')
    if [ -z "$DATA_ID" ] || [ "$DATA_ID" == "null" ]; then
        echo "Error: Failed to retrieve a valid Data ID."
        exit 1
    fi
    echo "File uploaded successfully. Data ID: $DATA_ID"

    # Poll for the scan results
    echo "Polling for scan results..."
    while true; do
        POLL_RESPONSE=$(curl -s -X GET "$METASCAN_API/file/$DATA_ID" \
            -H "apikey: $API_KEY" \
            -H "Content-Type: application/json")
        if [ $? -ne 0 ]; then
            echo "Error: Failed to poll for scan results."
            exit 1
        fi

        SCAN_STATUS=$(echo "$POLL_RESPONSE" | jq -r '.scan_results.scan_all_result_a')
        if [ "$SCAN_STATUS" != "In Progress" ]; then
            echo "Scan completed. Result: $SCAN_STATUS"
            break
        fi

        echo "Scan in progress. Retrying in $POLL_INTERVAL seconds..."
        sleep $POLL_INTERVAL
    done
fi

# Evaluate the scan result and pass/fail the build
if [ "$SCAN_STATUS" == "No Threat Detected" ]; then
    echo "Artifact is clean. Build passed."
    exit 0
else
    echo "Artifact scan failed: $SCAN_STATUS"
    exit 1
fi
